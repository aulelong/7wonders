import json

from Resources import Resources, ResourceType
from PlayerStatus import PlayerStatus
from Cards import CardsDB
from Wonder import WondersDB
from Move import MovesManager

class GameStatus():
	def __init__(self, gameStatusPath, cardsPath, wondersPath, playerNb):
		self.gameStatusPath = gameStatusPath
		self.cardsDB = CardsDB(cardsPath)
		self.wondersDB = WondersDB(wondersPath)
		self.playerNb = playerNb
		self.players = []
		self._stack = []
		for i in range(playerNb):
			self.players.append(PlayerStatus(i, self))
		self.loadFromFile()

	def getCardsDB(self):
		return self.cardsDB
	
	def getWondersDB(self):
		return self.wondersDB

	def getEra(self):
		return self.era
	
	def getTurn(self):
		return self.turn

	def getPlayerStatus(self, playerId):
		return self.players[playerId]
	
	def loadFromFile(self):
		with open(self.gameStatusPath, "r") as gameStatus:
			decodedStatus = json.load(gameStatus)
			self.clockwiseOrder = decodedStatus["game"]["clockwise"]
			self.era = decodedStatus["game"]["era"]
			self.turn = decodedStatus["game"]["turn"]
			for i in range(len(self.players)):
				self.players[i].importPlayerData(decodedStatus["players"][str(i)])

	def getMilitaryWinner(self):
		maxShields = 0
		idPlayer = 0
		for p in self.players:
			cur = Resources.getMaxValueOfTypeFromList(p.getResources(), ResourceType.SHIELDS)
			if(cur > maxShields):
				maxShields = cur
				idPlayer = p.getPlayerNumber()
		return idPlayer

	def buildStructure(cardName, playerStatus):
		pass

	def push(self, moves):
		for i in range(self.playerNb):
			if not MovesManager.isMoveLegal(moves[i], self.players[i]):
				return False
		for move in moves:
			if move.isBuildStructure():
				pass
			elif move.isBuildHandFree():
				pass
			elif move.isBuildWonder():
				pass
			elif move.isDiscard():
				pass
			else:
				return False

	def pop(self):
		pass

	def getInitialState(self):
		pass

	def getResult(self):
		pass