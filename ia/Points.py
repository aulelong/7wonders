from enum import Enum
from Resources import ResourceType

class PointsType(Enum):
	CIVILIAN = 0
	COMMERCIAL = 1
	GUILD = 2
	MILITARY = 4
	SCIENTIFIC = 6
	WONDER = 7

	def getPointsTypeFromCardType(cardType):
		from Cards import CardType
		if cardType == CardType.CIVILIAN:
			return PointsType.CIVILIAN
		if cardType == CardType.COMMERCIAL:
			return PointsType.COMMERCIAL
		if cardType == CardType.GUILD:
			return PointsType.GUILD
		if cardType == CardType.MILITARY:
			return PointsType.MILITARY
		if cardType == CardType.SCIENTIFIC:
			return PointsType.SCIENTIFIC
		

class Points():
	def __init__(self, civilian=0, commercial=0, guild=0, military=0, scientific=0, wonder=0):
		self.civilian = civilian
		self.commercial = commercial
		self.guild = guild
		self.military = military
		self.scientific = scientific
		self.wonder = wonder
	
	def getPointsByType(self, type):
		if type == PointsType.CIVILIAN:
			return self.civilian
		if type == PointsType.COMMERCIAL:
			return self.commercial
		if type == PointsType.GUILD:
			return self.guild
		if type == PointsType.MILITARY:
			return self.military
		if type == PointsType.SCIENTIFIC:
			return self.scientific
		if type == PointsType.WONDER:
			return self.wonder

	def addPointsByType(self, type, quantity):
		if type == PointsType.CIVILIAN:
			self.civilian += quantity
		elif type == PointsType.COMMERCIAL:
			self.commercial += quantity
		elif type == PointsType.GUILD:
			self.guild += quantity
		elif type == PointsType.MILITARY:
			self.military += quantity
		elif type == PointsType.SCIENTIFIC:
			self.scientific += quantity
		elif type == PointsType.WONDER:
			self.wonder += quantity
		return self

	def addPointsForCoins(self, resources):
		coins = resources[0].getResourceFromType(ResourceType.COINS)
		self.addPointsByType(PointsType.COMMERCIAL, int(coins / 3))
		return self
	
	#Ne renvoit pas toujours le plus au score possible, à cause de la merveille Babylone
	#A modifier pour renvoyer la bonne valeur
	def addPointsForScience(self, resources):
		compass = resources[0].getResourceFromType(ResourceType.COMPASS)
		gear = resources[0].getResourceFromType(ResourceType.GEAR)
		tablet = resources[0].getResourceFromType(ResourceType.TABLET)
		science = compass*compass + gear*gear + tablet*tablet
		science += min(min(compass, gear), tablet) * 7
		self.addPointsByType(PointsType.SCIENTIFIC, science)
		return self

	def addPointsForWonder(self, wonder, stage):
		p = wonder.getPointsForAllStageTo(stage).getPointsByType(PointsType.WONDER)
		self.addPointsByType(PointsType.WONDER, p)
		return self

	def getTotal(self):
		sum = 0
		for t in PointsType:
			sum += self.getPointsByType(t)
		return sum
	
	def sumPoints(points, otherPoints):
		sum = Points()
		for type in PointsType:
			sum.addPointsByType(type, points.getPointsByType(type))
			sum.addPointsByType(type, otherPoints.getPointsByType(type))
		return sum

	def getPointsFromCards(cards):
		output = Points()
		for c in cards:
			output = Points.sumPoints(output, c.getPoints())
		return output
	
	def print(self):
		for type in PointsType:
			print(type.name + " : " + str(self.getPointsByType(type)))