import argparse
import os
import time

from GameStatus import GameStatus
from heuristics import *
from RandomAI import *
from PriorityAI import *
from MilitaryAI import *
from WondersAI import *

gameStatusPath = "io/game_status.json"
cardsPath = "references/cards.csv"
wondersPath = "references/wonders.csv"
resultsPath = "results.txt"
aiResultsPath = "ia/results.csv"

playerNb = 3
playerFilePath1 = "io/player_1.json"
playerFilePath2 = "io/player_2.json"
playerFilePath3 = "io/player_3.json"

def dictKeysToString(inputdict):
    out = ""
    for i in range(len(inputdict.keys())):
        if(i != 0):
            if(i == len(inputdict.keys())-1):
                out += " or "
            else:
                out += ", "
        out += list(inputdict.keys())[i]
    return out

heuristicdict = {}

#To add an heuristic to the list, with heuriNameFunc the name of the heuristic in heuristics.py:
#heuristicdict["heuriName"] = heuriNameFunc

heuristicdict["simple"] = simpleHeuristic


aidictionnary = {}

#To add an AI to the list, with AINameClass the name of a class that extends WondersAI from WondersAI.py:
#aidictionnary["AIName"] = AINameClass

aidictionnary["random"] = RandomAI
aidictionnary["prio"] = PriorityAI
aidictionnary["military"] = MilitaryAI

AINames = dictKeysToString(aidictionnary)
heuriNames = dictKeysToString(heuristicdict)

parser = argparse.ArgumentParser(description="Launch an AI to play 7 Wonders",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-t", "--time", default=10, type=int, help="The duration of a turn for the AI")
parser.add_argument("-i", "--iterations", default=None, type=int, help="The number of games the AIs will play before giving you the statistics and results of the games. This argument can't be used with the GUI.")
parser.add_argument("-a","--ai", default="random", choices=aidictionnary.keys(), help=f"Type of AI to run as the first player ({AINames})")
parser.add_argument("-b", "--secondAi", choices=aidictionnary.keys(), help=f"Type of AI to run as a second player ({AINames}). If you don't specify this argument, you will need to play for player 2 and 3.")
parser.add_argument("-c", "--thirdAi", choices=aidictionnary.keys(), help=f"Type of AI to run as a third player ({AINames}). You need to have a second AI to be able to add a third. If you don't specify this argument, you will need to play for player 3.")
parser.add_argument("-l", "--heuri", default="simple", choices=heuristicdict.keys(), help=f"Type of heuristic the AI will use ({heuriNames})")
parser.add_argument("-m", "--secondHeuri", default="simple", choices=heuristicdict.keys(), help=f"Type of heuristic the second AI will use ({heuriNames})")
parser.add_argument("-n", "--thirdHeuri", default="simple", choices=heuristicdict.keys(), help=f"Type of heuristic the third AI will use ({heuriNames})")
args = vars(parser.parse_args())

gameStatusLastModified = os.path.getmtime(gameStatusPath)
game = GameStatus(gameStatusPath, cardsPath, wondersPath, playerNb)
iterations = args["iterations"]

"""if args["secondAi"] == None:
    player = HumanPlayer()
else:
    player = aidictionnary[args["secondAi"]](board._WHITE, heuristicdict[args["secondHeuri"]], args["time"])"""

AIs = []
Names = [args["ai"], args["secondAi"], args["thirdAi"]]

AIs.append(aidictionnary[Names[0]](len(AIs), playerFilePath1, game, heuristicdict[args["heuri"]], args["time"], iterations))

if args["secondAi"] != None:
    AIs.append(aidictionnary[Names[1]](len(AIs), playerFilePath2, game, heuristicdict[args["secondHeuri"]], args["time"], iterations))
    if args["thirdAi"] != None:
        AIs.append(aidictionnary[Names[2]](len(AIs), playerFilePath3, game, heuristicdict[args["thirdHeuri"]], args["time"], iterations))

def waitForNextTurn(lastModified):
    tmp = os.path.getmtime(gameStatusPath)
    while tmp == lastModified:
        tmp = os.path.getmtime(gameStatusPath)
        time.sleep(0.1)
    return tmp

def isGameOver(lastModified):
    if not os.path.exists(resultsPath):
        return False
    tmp = os.path.getmtime(resultsPath)
    if tmp != lastModified:
        return True
    return False
    
def getResults(gameId):
    mode = "w"
    if os.path.exists(aiResultsPath):
        mode = "a"
    with open(aiResultsPath, mode, newline="") as file:
            csvwriter = csv.writer(file, delimiter=",")
            if mode == "w":
                csvwriter.writerow(["gameId", "playerId", "aiType", "score"])
            with open(resultsPath, "r") as resultsData:
                lines = resultsData.readlines()
                for line in lines:
                    if line == "\n":
                        continue
                    playerId = int(line[7])
                    aiType = Names[playerId-1]
                    score = int(line.replace(" ", "").split(":")[1])
                    csvwriter.writerow([gameId, playerId, aiType, score])

if iterations == None:
    iterations = 1

for i in range(iterations):
    if not os.path.exists(resultsPath):
        resultsLastModified = 0
    else:
        resultsLastModified = os.path.getmtime(resultsPath)
    print("----- Beginning of the game -----")
    print(f"----- Era {game.getEra()} -----")
    #TODO Condition de fin de partie
    while(not isGameOver(resultsLastModified)):
        print(f"----- Turn {game.getTurn()} -----")
        #Checks the end of the era
        if len(game.players[0].getCardsHand()) != 0:
            for AIid in range(len(AIs)):
                AIs[AIid].playMoveWrapper()
                print(f"AI Player number {AIid} played its turn.")
            print("Every AI played, waiting for next turn...")
        else:
            print(f"----- Era {game.getEra()} -----")
        gameStatusLastModified = waitForNextTurn(gameStatusLastModified)
        game.loadFromFile()
    getResults(i)