from Resources import Resources
from Points import Points, PointsType

def resourcesAndPointsFromString(s):
	output = [[], Points(), "\"" in s]
	orList = s.split(" OR ")
	for ors in orList:
		r = Resources()
		andList = ors.split(" AND ")
		for ands in andList:
			tmpr = resourcesFromString(ands)[0]
			tmpp = pointsFromString(ands, PointsType.WONDER)
			r = Resources.addResources(r, tmpr)
			output[1] = Points.sumPoints(output[1], tmpp)
		output[0].append(r)
	return output
		

def resourcesFromString(resourcesString):
	output = []
	orList = resourcesString.split(" OR ")
	for ors in orList:
		r = Resources()
		andList = ors.split(" AND ")
		for ands in andList:
			if ands == "-":
				continue
			resource = ands.split(" ")
			try:
				quantity = int(resource[0].replace(" ", "").replace("x", "").replace("+", ""))
				r.addResourceFromString(resource[1], quantity)
			except:
				pass
		output.append(r)
	return output

def pointsFromString(pointsString, cardType):
	points = Points()
	if cardType == PointsType.WONDER:
		pointsType = cardType
	else:
		pointsType = PointsType.getPointsTypeFromCardType(cardType)
	try:
		quantity = int(pointsString.replace(" ", "").replace("+", "").replace("x", "").replace("Victorypoints", ""))
		points.addPointsByType(pointsType, quantity)
	except:
		pass
	return points