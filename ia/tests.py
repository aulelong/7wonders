from colorama import Fore

def testSampleFalse():
    return False

def testSampleTrue():
    return True

#TODO
def testMoveEq():
    return False

#TODO
def testMovesManagerIsMoveLegal():
    return False

#TODO
def testGameStatusPush():
    return False

#TODO
def testGameStatusPop():
    return False

funcDict = {
    "Sample.False()" : testSampleFalse,
    "Sample.True()" : testSampleTrue,
    "Move.__eq__()": testMoveEq,
    "MovesManager.isMoveLegal()": testMovesManagerIsMoveLegal,
    "GameStatus.push()": testGameStatusPush,
    "GameStatus.pop()": testGameStatusPop
}

#Eventuellement ajouter un message en cas d'erreur ?
print("------ Testing begin ------")
for name, func in funcDict.items():
    print()
    print(Fore.WHITE + "Testing " + name + " ...")
    if func():
        print(Fore.GREEN + name + " successful.")
    else:
        print(Fore.RED + name + " unsuccessful.")
print()
print(Fore.WHITE + "------ End of testing ------")
