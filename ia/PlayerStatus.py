from Resources import Resources
from Move import MovesManager
from Points import Points, PointsType

class PlayerStatus():
	def __init__(self, playerNb, gameStatus):
		self.playerNb = playerNb
		self.gameStatus = gameStatus

	def refreshPoints(self, playerData):
		self.points = Points.getPointsFromCards(self.playedCards)
		self.points = self.points.addPointsForCoins(self.resources)
		self.points = self.points.addPointsForScience(self.resources)
		self.points = self.points.addPointsForWonder(self.wonder, self.wonderStage)
		self.points = self.points.addPointsByType(PointsType.MILITARY, playerData["points"]["military"])

	def refreshResources(self, playerData):
		self.resources = Resources.addResourceFromCards([Resources()], self.playedCards)
		self.resources = Resources.addResourcesToListResources(self.resources, Resources(coins=playerData["resources"]["coins"]))
		self.resources = Resources.addResourcesFromWonder(self.resources, self.wonder, self.wonderStage)

	def importPlayerData(self, playerData):
		self.cardsHand = []
		self.playableCards = []
		self.playedCards = []
		self.resources = [Resources()]
		self.points = Points()
		self.canBuildFree = playerData["can_build_hand_free"]
		self.canBuildWonder = playerData["can_build_wonder"]
		for cardName in playerData["cards_hand"]:
			self.cardsHand.append(self.gameStatus.getCardsDB().getCardByName(cardName))
		for cardName in playerData["cards_playable"]:
			self.playableCards.append(self.gameStatus.getCardsDB().getCardByName(cardName))
		for cardName in playerData["cards_played"]:
			self.playedCards.append(self.gameStatus.getCardsDB().getCardByName(cardName))
		self.wonder = self.gameStatus.getWondersDB().getWonderByName(playerData["wonder_name"])
		self.wonderStage = playerData["wonder_stage"]
		#self.wonder.print()
		#print("Player " + str(self.playerNb))
		self.refreshResources(playerData)
		#Resources.printList(self.resources)
		self.refreshPoints(playerData)
		#self.points.print()
		#print()
		self.legalMoves = MovesManager.getLegalMoves(self)
	
	def getPlayerNumber(self):
		return self.playerNb

	def getCanBuildFree(self):
		return self.canBuildFree

	def getCanBuildWonder(self):
		return self.canBuildWonder

	def getCardsHand(self):
		return self.cardsHand

	def getPlayableCards(self):
		return self.playableCards
	
	def getPlayedCards(self):
		return self.playedCards
	
	def getResources(self):
		return self.resources
	
	def getLegalMoves(self):
		return self.legalMoves
	
	def getWonder(self):
		return self.wonder
	
	def isPlayerMilitaryWinning(self):
		return self.playerNb == self.gameStatus.getMilitaryWinner()
	

