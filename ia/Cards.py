from enum import Enum
import csv

import Utility

class CardType(Enum):
	CIVILIAN = 0
	COMMERCIAL = 1
	GUILD = 2
	MANUFACTURED_GOODS = 3
	MILITARY = 4
	RAW_MATERIAL = 5
	SCIENTIFIC = 6

	def fromString(typeString):
		table = {
			"Civilian Structure" : CardType.CIVILIAN,
			"Commercial Structure" : CardType.COMMERCIAL,
			"Guild" : CardType.GUILD,
			"Manufactured Good" : CardType.MANUFACTURED_GOODS,
			"Military Structure" : CardType.MILITARY,
			"Raw Material" : CardType.RAW_MATERIAL,
			"Scientific Structure" : CardType.SCIENTIFIC}
		return table[typeString]

class Card():
	def __init__(self, id, name, cardType, cost, prod, points, futureFree, freeWith, age):
		self.id = id
		self.name = name
		self.cardType = cardType
		self.cost = cost
		self.prod = prod
		self.points = points
		self.futureFree = futureFree
		self.freeWith = freeWith
		self.age = age
	
	def getName(self):
		return self.name
	
	def getCardType(self):
		return self.cardType
	
	def getProd(self):
		return self.prod
	
	def getPoints(self):
		return self.points
	
	#pas sure que ça fonctionne comme il faut (bool)
	def getIsFutureFree(self):
		return self.futureFree != "-"

	def getIsFreeWith(self):
		return self.freeWith != "-"
	
	def getIsFree(self):
		return self.cost == "-"
	
class CardsDB():
	def __init__(self, path):		
		self.cards = {}
		with open(path, newline='') as csvfile:
			cardreader = csv.reader(csvfile, delimiter=',')
			for row in cardreader:
				if not "3" in row[8] and row[8] != "-":
					continue
				id = int(row[0])
				name = row[1]
				cardType = CardType.fromString(row[2])
				cost = Utility.resourcesFromString(row[3])
				prod = Utility.resourcesFromString(row[4])
				points = Utility.pointsFromString(row[4], cardType)
				futureFree = row[5]
				freeWith = row[6]
				age = int(row[7])
				self.cards[name] = Card(id, name, cardType, cost, prod, points, futureFree, freeWith, age)

	def getCardByName(self, name):
		return self.cards[name]
