from enum import Enum

class Subcommands(Enum):
    BUILDSTRUCTURE = 0
    BUILDHANDFREE = 1
    BUILDWONDER = 2
    DISCARD = 3

    def subcommandToString(self):
        if self == Subcommands.BUILDSTRUCTURE:
            return "build_structure"
        elif self == Subcommands.BUILDHANDFREE:
            return "build_hand_free"
        elif self == Subcommands.BUILDWONDER:
            return "build_wonder"
        else:
            return "discard"

class Move():
    def __init__(self, subcommand, argument, extra=""):
        self.subcommand = subcommand
        self.argument = argument
        self.extra = extra

    def getSubcommand(self):
        return self.subcommand
    
    def getArgument(self):
        return self.argument
    
    def getExtra(self):
        return self.extra
    
    def isBuildStructure(self):
        return self.getSubcommand() == Subcommands.BUILDSTRUCTURE
    
    def isBuildHandFree(self):
        return self.getSubcommand() == Subcommands.BUILDHANDFREE
    
    def isBuildWonder(self):
        return self.getSubcommand() == Subcommands.BUILDWONDER
    
    def isDiscard(self):
        return self.getSubcommand() == Subcommands.DISCARD
    
    def __eq__(self, other):
        if not isinstance(other, Move):
            return NotImplemented
        return self.subcommand == other.subcommand and self.argument == other.argument and self.extra == other.extra

class MovesManager():
    def getLegalMoves(playerStatus):
        moves = []

        for card in playerStatus.getPlayableCards():
            moves.append(Move(Subcommands.BUILDSTRUCTURE, card.getName()))

        if playerStatus.getCanBuildFree():
            for card in playerStatus.getCardsHand():
                moves.append(Move(Subcommands.BUILDHANDFREE, card.getName()))
        
        if playerStatus.getCanBuildWonder():
            for card in playerStatus.getCardsHand():
                moves.append(Move(Subcommands.BUILDWONDER, card.getName()))

        for card in playerStatus.getCardsHand():
            moves.append(Move(Subcommands.DISCARD, card.getName()))

        return moves

    def isMoveLegal(move, playerStatus):
        for m in playerStatus.getLegalMoves():
            if m == move:
                return True
        return False
    
    def getBuildStructureMoves(legalMoves):
        buildStructure = []
        for move in legalMoves:
            if move.getSubcommand() == Subcommands.BUILDSTRUCTURE:
                buildStructure.append(move)
        return buildStructure
    
    def getBuildFreeMoves(legalMoves):
        buildFree = []
        for move in legalMoves:
            if move.getSubcommand() == Subcommands.BUILDHANDFREE:
                buildFree.append(move)
        return buildFree
    
    def getBuildWonderMoves(legalMoves):
        buildWonder = []
        for move in legalMoves:
            if move.getSubcommand() == Subcommands.BUILDWONDER:
                buildWonder.append(move)
        return buildWonder
    
    def getDiscardMoves(legalMoves):
        discard = []
        for move in legalMoves:
            if move.getSubcommand() == Subcommands.DISCARD:
                discard.append(move)
        return discard