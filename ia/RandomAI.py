import random
from WondersAI import *

class RandomAI(WondersAI):
	def playMove(self):
		cards = self.playerStatus.getPlayableCards()
		cardsNb = len(cards)
		subcommand = Subcommands.BUILDSTRUCTURE
		if cardsNb == 0:
			cards = self.playerStatus.getCardsHand()
			cardsNb = len(cards)
			subcommand = Subcommands.DISCARD
		toPlay = cards[random.randint(0, len(cards)-1)].getName()
		self.writeInFile(Move(subcommand, toPlay))