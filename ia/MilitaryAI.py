from WondersAI import *
from Cards import *

class MilitaryAI(WondersAI):

    def madScientist(self,cards,age):
        toPlay = cards[0].getName()
        for i in range(len(cards)):
            cardType = cards[i].getCardType()
            if(cardType == CardType.RAW_MATERIAL or cardType == CardType.SCIENTIFIC or cardType == CardType.MANUFACTURED_GOODS):
                toPlay = cards[i].getName()
                break
        return toPlay
    
    def military(self,cards,age):
        militaryWinning = self.playerStatus.isPlayerMilitaryWinning()
        playableCards = self.playerStatus.getPlayableCards()
        toPlay = cards[0].getName()
      
        #tour normal
        for i in range(len(playableCards)):
            cardType = playableCards[i].getCardType()
            if(cardType == CardType.CIVILIAN or cardType == CardType.COMMERCIAL):
                toPlay = playableCards[i].getName()
                if(playableCards[i].getIsFutureFree or playableCards[i].getIsFreeWith):
                    break

        if(age == 2):
            #acquisition ressources nécessaire
            for i in range(len(playableCards)):
                cardType = playableCards[i].getCardType()
                if(cardType == CardType.RAW_MATERIAL):
                    toPlay = playableCards[i].getName()
                    break       

        #victoire militaire modérée
        if(not militaryWinning):
            for i in range(len(playableCards)):
                cardType = playableCards[i].getCardType()
                if(cardType == CardType.MILITARY):
                    toPlay = playableCards[i].getName()
                    if(playableCards[i].getIsFutureFree or playableCards[i].getIsFree):
                        break

        return toPlay


    def playMove(self):
        militaryWinning = self.playerStatus.isPlayerMilitaryWinning()
        cards = self.playerStatus.getCardsHand()
        playableCards = self.playerStatus.getPlayableCards()
        age = self.gameStatus.getEra()
        subcommand = Subcommands.BUILDSTRUCTURE
        toPlay = cards[0].getName()
        wonder = self.playerStatus.getWonder()
        scientist = {"Apothecary","Workshop","Scriptorium","Dispensary","Laboratory","Library","School","Lodge","Observatory","University","Academy","Study","Loom","Glassworks","Press"}
        if(len(playableCards) == 0 or (militaryWinning and self.playerStatus.canBuildWonder)):
            if(self.playerStatus.canBuildWonder):
                subcommand = Subcommands.BUILDWONDER
            else:
                subcommand = Subcommands.DISCARD
        else:
            if(wonder == "Babylon A" or wonder == "Babylon B"):
                toPlay = self.madScientist(cards,age)

            else:
                toPlay = self.military(cards,age)
                for card in scientist:
                    if(toPlay == card):
                        if(self.playerStatus.canBuildWonder):
                            subcommand = Subcommands.BUILDWONDER
                        else:
                            subcommand = Subcommands.DISCARD

        self.writeInFile(Move(subcommand, toPlay))
		