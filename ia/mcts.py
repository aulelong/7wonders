import random
import math
import numpy as np

from collections import defaultdict
from main import *

class MCTS_Node:
    def __init__(self, state, parent=None, parent_action=None):
        self.state = state
        self.parent = parent
        self.parent_action = parent_action
        self.children = []
        self._number_of_visits = 0
        self._results = defaultdict(int)
        self._results[1] = 0
        self._results[-1] = 0
        self._untried_actions = None
        self._untried_actions = self.untried_actions()
        return
    
    def untried_actions(self):
        self._untried_actions = MovesManager.getLegalMoves(self.state)
        random.shuffle(self._untried_actions)
        return self._untried_actions
    
    def q(self):
        wins = self._results[1]
        loses = self._results[-1]
        return wins - loses
    
    def n(self):
        return self._number_of_visits
    
    def expand(self):
        #pop n'est pas implémenté
        action = GameStatus.pop(self._untried_actions)

        #push n'est pas implémenté
        next_state = GameStatus.push(self.state, action)

        child_node = MCTS_Node(
            next_state, parent=self, parent_action=action)

        self.children.append(child_node)
        return child_node
    
    def is_terminal_node(self):
        #fonction de main.py
        return isGameOver(self.state)
    
    def rollout(self):
        current_rollout_state = self.state
        while not isGameOver(current_rollout_state):
            possible_moves = MovesManager.getLegalMoves(current_rollout_state)
            action = random.choice(possible_moves)
            current_rollout_state = GameStatus.push(current_rollout_state, action)
        #getResult() n'est pas implémenté
        return GameStatus.getResult(current_rollout_state)

    def backpropagate(self, result):
        self._number_of_visits += 1
        self._results[result] += 1
        if self.parent:
            self.parent.backpropagate(result)

    def is_fully_expanded(self):
        return len(self._untried_actions) == 0

    #c_param est un paramètre d'exploration, il est souvent fixé à sqrt(2) soit ~1.4
    def best_child(self, c_param=1.4):
        choices_weights = [
            (c.q() / c.n()) + c_param * math.sqrt((2 * math.log(self.n()) / c.n()))
            for c in self.children
        ]
        return self.children[np.argmax(choices_weights)]
    
    def rollout_policy(self, possible_moves):
        return random.choice(possible_moves)
    
    def _tree_policy(self):
        current_node = self
        while not current_node.is_terminal_node():
            if not current_node.is_fully_expanded():
                return current_node.expand()
            else:
                current_node = current_node.best_child()
        return current_node
    
    def best_action(self):
        simulation_no = 100
        for _ in range(simulation_no):
            v = self._tree_policy()
            reward = v.rollout()
            v.backpropagate(reward)
        return self.best_child(c_param=0.)
    
def main():
    #getInitialState() n'est pas implémenté
    root = MCTS_Node(state = GameStatus.getInitialState())
    selected_node = root.best_action()
    return 