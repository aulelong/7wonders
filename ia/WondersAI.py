import json
import time
from Move import Subcommands, Move, MovesManager

class WondersAI():
    def __init__(self, playerId, playerFilePath, gameStatus, heuristic, time, iterations):
        self.playerId = playerId
        self.playerFilePath = playerFilePath
        self.gameStatus = gameStatus
        self.playerStatus = self.gameStatus.getPlayerStatus(playerId)
        self.heuristic = heuristic
        self.iterations = iterations
        self.shouldWriteReady = True
        if iterations == None:
            self.shouldWriteReady = False
        #self.infinity = 2**20 #Choisi pour être arbitrairement grand
        #self.maxDepth = 15 #Choisi pour être arbitrairement grand
        #self.maxTime = time #Temps pour trouver son coup
        #self.startTime = 0

    def isTimeUp(self):
        return (time.time() - self.startTime >= self.maxTime)

    def writeInFile(self, move):
        toWrite = {
            "command" : {
                "subcommand" : move.getSubcommand().subcommandToString(),
                "argument" : move.getArgument(),
                "extra" : move.getExtra()
            }
        }
        with open(self.playerFilePath, "w") as file:
            json.dump(toWrite, file)

    def writeReady(self):
        with open("io/ready.txt", "a") as file:
            file.write("ready\n")

    def playMove(self):
        pass

    def playMoveWrapper(self):
        self.playMove()
        if self.shouldWriteReady:
            self.writeReady()

    