import csv
import Utility
from Resources import Resources
from Points import Points

class Wonder():
	def __init__(self, id, fullName, name, side, stageNb, baseProd, firstStageCost, firstStageProd, firstStagePoints, firstStageHasEffect, secondStageCost, secondStageProd, secondStagePoints, secondStageHasEffect, thirdStageCost, thirdStageProd, thirdStagePoints, thirdStageHasEffect, fourthStageCost, fourthStageProd, fourthStagePoints, fourthStageHasEffect):
		self.id = id
		self.fullName = fullName
		self.name = name
		self.side = side
		self.stageNb = stageNb
		self.baseProd = baseProd
		self.firstStageCost = firstStageCost
		self.firstStageProd = firstStageProd
		self.firstStagePoints = firstStagePoints
		self.firstStageHasEffect = firstStageHasEffect
		self.secondStageCost = secondStageCost
		self.secondStageProd = secondStageProd
		self.secondStagePoints = secondStagePoints
		self.secondStageHasEffect = secondStageHasEffect
		self.thirdStageCost = thirdStageCost
		self.thirdStageProd = thirdStageProd
		self.thirdStagePoints = thirdStagePoints
		self.thirdStageHasEffect = thirdStageHasEffect
		self.fourthStageCost = fourthStageCost
		self.fourthStageProd = fourthStageProd
		self.fourthStagePoints = fourthStagePoints
		self.fourthStageHasEffect = fourthStageHasEffect
	
	def getId(self):
		return self.id
	
	def getBaseProd(self):
		return self.baseProd

	def getFirstStageProd(self):
		return self.firstStageProd
	
	def getSecondStageProd(self):
		return self.secondStageProd
	
	def getThirdStageProd(self):
		return self.thirdStageProd
	
	def getFourthStageProd(self):
		return self.fourthStageProd
	
	def getFirstStagePoints(self):
		return self.firstStagePoints
	
	def getSecondStagePoints(self):
		return self.secondStagePoints
	
	def getThirdStagePoints(self):
		return self.thirdStagePoints
	
	def getFourthStagePoints(self):
		return self.fourthStagePoints
	
	def getPointsForStage(self, stageNb):
		funcs = [self.getFirstStagePoints, self.getSecondStagePoints, self.getThirdStagePoints, self.getFourthStagePoints]
		return funcs[stageNb-1]()
	
	def getPointsForAllStageTo(self, stageNb):
		output = Points()
		for i in range(stageNb):
			output = Points.sumPoints(output, self.getPointsForStage(i+1))
		return output
	
	def getProdForStage(self, stageNb):
		funcs = [self.getFirstStageProd, self.getSecondStageProd, self.getThirdStageProd, self.getFourthStageProd]
		return funcs[stageNb-1]()
	
	def getProdForAllStageTo(self, stageNb):
		output = self.getBaseProd()
		for i in range(stageNb):
			p = self.getProdForStage(i+1)
			output = Resources.mergeListsOfResources(output, p)
		return output
	
	def print(self):
		print("ID = " + str(self.id))
		print("FULLNAME = " + self.fullName)
		print("NAME = " + self.name)
		print("SIDE = " + self.side)
		print("STAGE NB = " + str(self.stageNb))
		print("BASE PROD = ")
		Resources.printList(self.baseProd)
		print("FIRST STAGE COST = ")
		Resources.printList(self.firstStageCost)
		print("FIRST STAGE PROD = ")
		Resources.printList(self.firstStageProd)
		self.firstStagePoints.print()
		print("HAS EFFECT = " + str(self.firstStageHasEffect))
		print("SECOND STAGE COST = ")
		Resources.printList(self.secondStageCost)
		print("SECOND STAGE PROD = ")
		Resources.printList(self.secondStageProd)
		self.secondStagePoints.print()
		print("HAS EFFECT = " + str(self.secondStageHasEffect))
		print("THIRD STAGE COST = ")
		Resources.printList(self.thirdStageCost)
		print("THIRD STAGE PROD = ")
		Resources.printList(self.thirdStageProd)
		self.thirdStagePoints.print()
		print("HAS EFFECT = " + str(self.thirdStageHasEffect))
		print("FOURTH STAGE COST = ")
		Resources.printList(self.fourthStageCost)
		print("FOURTH STAGE PROD = ")
		Resources.printList(self.fourthStageProd)
		self.fourthStagePoints.print()
		print("HAS EFFECT = " + str(self.fourthStageHasEffect))

class WondersDB():
	def __init__(self, path):
		self.wonders = {}
		with open(path, newline='') as csvfile:
			wonderreader = csv.reader(csvfile, delimiter=',')
			for row in wonderreader:
				if row[0] == "id":
					continue
				id = int(row[0])
				fullName = row[1]
				name = row[2]
				side = row[3]
				baseProd = Utility.resourcesFromString(row[4])
				firstStageCost = Utility.resourcesFromString(row[5])
				firstStageProd, firstStagePoints, firstStageHasEffect = Utility.resourcesAndPointsFromString(row[6])				
				secondStageCost = Utility.resourcesFromString(row[7])
				secondStageProd, secondStagePoints, secondStageHasEffect = Utility.resourcesAndPointsFromString(row[8])
				thirdStageCost = Utility.resourcesFromString(row[9])
				thirdStageProd, thirdStagePoints, thirdStageHasEffect = Utility.resourcesAndPointsFromString(row[10])
				fourthStageCost = Utility.resourcesFromString(row[11])
				fourthStageProd, fourthStagePoints, fourthStageHasEffect = Utility.resourcesAndPointsFromString(row[12])
				stageNb = 4
				if row[11] == "-":
					stageNb -= 1
				if row[9] == "-":
					stageNb -= 1
				self.wonders[name + " " + side] = Wonder(id, fullName, name, side, stageNb, baseProd, firstStageCost, firstStageProd, firstStagePoints, firstStageHasEffect, secondStageCost, secondStageProd, secondStagePoints, secondStageHasEffect, thirdStageCost, thirdStageProd, thirdStagePoints, thirdStageHasEffect, fourthStageCost, fourthStageProd, fourthStagePoints, fourthStageHasEffect)
	
	def getWonderByName(self, name):
		return self.wonders[name]
	
	def getWonderById(self, id):
		for wonder in self.wonders.values():
			if wonder.getId() == id:
				return wonder