from WondersAI import *
from Cards import *

class PriorityAI(WondersAI):

    def age1(self,cards):
        toPlay = cards[0].getName()
        cardType = cards[0].getCardType()
        for i in range(len(cards)):
            cardType = cards[i].getCardType()
            if(cardType == CardType.RAW_MATERIAL or cardType == CardType.SCIENTIFIC or cardType == CardType.MANUFACTURED_GOODS):
                toPlay = cards[i].getName()
                break
        return toPlay
    
    def age2(self,cards):
        toPlay = cards[0].getName()
        cardType = cards[0].getCardType()
        for i in range(len(cards)):
            cardType = cards[i].getCardType()
            if(cardType == CardType.RAW_MATERIAL or cardType == CardType.SCIENTIFIC or cardType == CardType.MANUFACTURED_GOODS):
                toPlay = cards[i].getName()
                break
        return toPlay
    def age3(self,cards):
        toPlay = cards[0].getName()
        cardType = cards[0].getCardType()
        for i in range(len(cards)):
            cardType = cards[i].getCardType()
            if(cardType == CardType.GUILD or cardType == CardType.SCIENTIFIC):
                toPlay = cards[i].getName()
                break
        return toPlay


    def playMove(self):
        """ derniere carte de chaque age = merveille
        AGE 1 - materiaux de base utile à la merveille
            - materiaux non possédé
            - matériaux gris
            - casernes si on a rhodos sinon cartes vertes

        AGE 2 - cartes achetables
                - casernes si rhodos et competition
                - vertes sinon
        AGE 3 - guildes
            - vertes

        """
        cards = self.playerStatus.getCardsHand()
        playableCards = self.playerStatus.getPlayableCards()
        age = self.gameStatus.getEra()
        cardsNb = len(cards)
        subcommand = Subcommands.BUILDSTRUCTURE

        if (cardsNb == 2) and (self.playerStatus.canBuildWonder == True):
            subcommand = Subcommands.BUILDWONDER
            toPlay = cards[0].getName()
        else:
            if(len(playableCards) == 0):
                subcommand = Subcommands.DISCARD
            if(age == 1):
                toPlay = self.age1(cards)
            elif(age == 2):
                toPlay = self.age2(cards)
            else:
                toPlay = self.age3(cards)
        self.writeInFile(Move(subcommand, toPlay))
		