import copy
from enum import Enum

class ResourceType(Enum):
	CLAY = 0
	COINS = 1
	COMPASS = 2
	GEAR = 3
	GLASS = 4
	LOOM = 5
	ORE = 6
	PAPYRUS = 7
	SHIELDS = 9
	STONE = 10
	TABLET = 11
	WOOD = 12

class Resources():
	def __init__(self, clay=0, coins=0, compass=0, gear=0, glass=0, loom=0, ore=0, papyrus=0, shields=0, stone=0, tablet=0, wood=0):
		self.clay = clay
		self.coins = coins
		self.compass = compass
		self.gear = gear
		self.glass = glass
		self.loom = loom
		self.ore = ore
		self.papyrus = papyrus
		self.shields = shields
		self.stone = stone
		self.tablet = tablet
		self.wood = wood

	def getResourceFromType(self, resourceType):
		if resourceType == ResourceType.CLAY:
			return self.clay
		elif resourceType == ResourceType.COINS:
			return self.coins
		elif resourceType == ResourceType.COMPASS:
			return self.compass
		elif resourceType == ResourceType.GEAR:
			return self.gear
		elif resourceType == ResourceType.GLASS:
			return self.glass
		elif resourceType == ResourceType.LOOM:
			return self.loom
		elif resourceType == ResourceType.ORE:
			return self.ore
		elif resourceType == ResourceType.PAPYRUS:
			return self.papyrus
		elif resourceType == ResourceType.SHIELDS:
			return self.shields
		elif resourceType == ResourceType.STONE:
			return self.stone
		elif resourceType == ResourceType.TABLET:
			return self.tablet
		elif resourceType == ResourceType.WOOD:
			return self.wood

	def addSingleResource(self, resourceType, quantity):
		if resourceType == ResourceType.CLAY:
			self.clay += quantity
		elif resourceType == ResourceType.COINS:
			self.coins += quantity
		elif resourceType == ResourceType.COMPASS:
			self.compass += quantity
		elif resourceType == ResourceType.GEAR:
			self.gear += quantity
		elif resourceType == ResourceType.GLASS:
			self.glass += quantity
		elif resourceType == ResourceType.LOOM:
			self.loom += quantity
		elif resourceType == ResourceType.ORE:
			self.ore += quantity
		elif resourceType == ResourceType.PAPYRUS:
			self.papyrus += quantity
		elif resourceType == ResourceType.SHIELDS:
			self.shields += quantity
		elif resourceType == ResourceType.STONE:
			self.stone += quantity
		elif resourceType == ResourceType.TABLET:
			self.tablet += quantity
		elif resourceType == ResourceType.WOOD:
			self.wood += quantity
		return self

	def addResources(resource, otherResource):
		sum = Resources()
		for type in ResourceType:
			sum.addSingleResource(type, resource.getResourceFromType(type))
			sum.addSingleResource(type, otherResource.getResourceFromType(type))
		return sum

	def addResourcesToListResources(resourceList, resource):
		output = copy.copy(resourceList)
		for i in range(len(output)):
			output[i] = Resources.addResources(output[i], resource)
		return output

	def cartesianProduct(resourceList, otherResourceList):
		output = []
		if resourceList == []:
			return otherResourceList
		if otherResourceList == []:
			return resourceList
		for r in resourceList:
			for o in otherResourceList:
				"""for type in ResourceType:
					quantity = o.getResourceFromType(type)
					if quantity != 0:
						tmp = copy.copy(r)
						tmp.addSingleResource(type, quantity)
						output.append(tmp)"""
				tmp = Resources.addResources(r, o)
				output.append(tmp)
		return output

	def mergeListsOfResources(resourceList, otherResourceList):
		if len(resourceList) == 1:
			output = Resources.addResourcesToListResources(otherResourceList, resourceList[0])
		elif len(otherResourceList) == 1:
			output = Resources.addResourcesToListResources(resourceList, otherResourceList[0])
		else:
			output = Resources.cartesianProduct(resourceList, otherResourceList)
		return output

	def addResourceFromString(self, resourceString, quantity):
		resourceString = resourceString.lower()
		if "clay" in resourceString:
			self.clay += quantity
		elif "coin" in resourceString:
			self.coins += quantity
		elif "compass" in resourceString:
			self.compass += quantity
		elif "gear" in resourceString:
			self.gear += quantity
		elif "glass" in resourceString:
			self.glass += quantity
		elif "loom" in resourceString or "cloth" in resourceString:
			self.loom += quantity
		elif "ore" in resourceString:
			self.ore += quantity
		elif "papyrus" in resourceString or "press" in resourceString:
			self.papyrus += quantity
		elif "shield" in resourceString:
			self.shields += quantity
		elif "stone" in resourceString:
			self.stone += quantity
		elif "tablet" in resourceString:
			self.tablet += quantity
		elif "wood" in resourceString:
			self.wood += quantity
		else:
			assert False, ("Wrong resource name : " + resourceString)

	def addResourceFromDict(self, resourceDict):
		for resourceName in resourceDict:
			quantity = resourceDict[resourceName]
			if quantity == 0:
				continue
			self.addResourceFromString(resourceName, quantity)

	def addResourceFromCards(resource, cards):
		output = resource
		for card in cards:
			prod = card.getProd()
			output = Resources.mergeListsOfResources(output, prod)
		return output
	
	def addResourcesFromWonder(resource, wonder, stage):
		output = resource
		prod = wonder.getProdForAllStageTo(stage)
		output = Resources.mergeListsOfResources(output, prod)
		return output

	def getMinsOfTypeFromList(resourceList, type):
		min = 999999999
		output = []
		for r in resourceList:
			quantity = r.getResourceFromType(type)
			if quantity < min:
				min = quantity
				output = [r]
			elif quantity == min:
				output.append(r)
		return output
	
	def getMinValueOfTypeFromList(resourceList, type):
		r = Resources.getMinsOfTypeFromList(resourceList, type)[0]
		return r.getResourceFromType(type)
	
	def getMaxsOfTypeFromList(resourceList, type):
		max = -1
		output = []
		for r in resourceList:
			quantity = r.getResourceFromType(type)
			if quantity > max:
				max = quantity
				output = [r]
			elif quantity == max:
				output.append(r)
		return output
	
	def getMaxValueOfTypeFromList(resourceList, type):
		r = Resources.getMaxsOfTypeFromList(resourceList, type)[0]
		return r.getResourceFromType(type)
	
	def printList(resourceList):
		print("------")
		for r in resourceList:
			r.print()
			print("------")

	def print(self):
		for type in ResourceType:
			print(type.name + " : " + str(self.getResourceFromType(type)))