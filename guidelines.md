# Guidelines

All attributes and methods will be written in English.
Functions should start with an action, written in CamelCase;
e.g., function "buy resource":
<type> PurchaseResource(args);

Attributes should be written in snake_case;
e.g., attribute "conflict markers":
<type> conflict_markers;

# References
* [Rules](https://waa.ai/O48v)
* [Quick rules](https://waa.ai/O48z)
* [Information about cards and wonders (tabulated)](https://github.com/dmag-ufsm/Game/tree/master/references)

# Issues encountered:
Encountered an issue? Open an
[issue](https://github.com/dmag-ufsm/Game/issues)! Check open and closed issues;
feel free to comment again on a closed issue if you encounter a supposedly fixed bug.

* Warnings:
    * Pay attention to cards that can be built for free in case of combos.
      This may be mistaken for a bug, but it isn't.
    * Pay attention to duplicate cards. When a card is played again, it is
      discarded and the player's coin count increases. This may be
      mistaken for a bug, but it isn't.

# TODO
* Check if wonder stage can be built or not. Logic
  similar to what was done to check if a card is playable or not.
    * Done! To check if a player can build the wonder stage or not in the current turn,
    just check the state of the variable
    ```player[i]->can_build_wonder```.

* Read previous game state from file
* Write about dependencies